package dev.inacap.holamundoapr4vg2020.controladores;

import android.content.ContentValues;
import android.content.Context;

import dev.inacap.holamundoapr4vg2020.modelos.BDPrincipalContract;
import dev.inacap.holamundoapr4vg2020.modelos.UsuariosModelo;

public class UsuariosControlador {
    private UsuariosModelo capaModelo;

    public UsuariosControlador(Context context){
        this.capaModelo = new UsuariosModelo(context);
    }

    public void crearUsuario(String nombreUsuario, String password){
        // Crear objeto
        ContentValues usuarioNuevo = new ContentValues();
        usuarioNuevo.put(BDPrincipalContract.BDPrincipalUsuarios.COLUMNA_NOMBREUSUARIO, nombreUsuario);
        usuarioNuevo.put(BDPrincipalContract.BDPrincipalUsuarios.COLUMNA_PASSWORD, password);

        // Enviar a la capa modelo
        this.capaModelo.crearUsuario(usuarioNuevo);
    }

    public boolean procesarLogin(String nombreUsuario, String password){
        // Buscar al usuario en la BD
        ContentValues usuarioBD = this.capaModelo.obtenerUsuario(nombreUsuario);

        // Validar que el usuario exista
        if(usuarioBD == null){
            // Usuario no existe en la BD

            return false;
        }

        // El usuario existe
        // Ahora debemos validar la password
        String passBD = usuarioBD.getAsString(BDPrincipalContract.BDPrincipalUsuarios.COLUMNA_PASSWORD);
        if(passBD.equals(password)){
            // la persona inicio sesion
            return true;
        }

        // Por defecto la persona no inicia sesion
        return false;
    }
}
