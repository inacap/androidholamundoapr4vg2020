package dev.inacap.holamundoapr4vg2020.vistas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import dev.inacap.holamundoapr4vg2020.MainActivity;
import dev.inacap.holamundoapr4vg2020.R;
import dev.inacap.holamundoapr4vg2020.vistas.fragmentos.BienvenidaFragment;

public class HomeActivity extends AppCompatActivity implements BienvenidaFragment.OnFragmentInteractionListener {

    private Button btLogout, btCambiar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        this.btLogout = findViewById(R.id.btLogout);
        this.btCambiar = findViewById(R.id.btCambiar);

        this.btLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Guardar el inicio de sesion
                SharedPreferences sesion = getSharedPreferences("sesion", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sesion.edit();

                // Agregar la variable
                editor.putBoolean("yaInicioSesion", false);

                // guardar los datos
                editor.commit();

                // Abrir nueva Activity
                Intent intentHomeActivity = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(intentHomeActivity);
                finish();
            }
        });

        this.btCambiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // mostrar el Fragment

                // Instanciar el fragment a mostrar
                BienvenidaFragment fragBienvenida = new BienvenidaFragment();

                // Administrador de fragmentos
                FragmentManager manager = getSupportFragmentManager();

                // Asociar el fragment al contenedor
                manager.beginTransaction().replace(R.id.fgContenedor, fragBienvenida).commit();
            }
        });
    }

    @Override
    public void onFragmentInteraction(String nombreFragment, String nombreEvento) {
        if(nombreFragment.equals("BienvenidaFragment")){
            if(nombreEvento.equals("BTPRUEBA_CLICK")){
                Toast.makeText(getApplicationContext(), "Hola desde la Activity", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
