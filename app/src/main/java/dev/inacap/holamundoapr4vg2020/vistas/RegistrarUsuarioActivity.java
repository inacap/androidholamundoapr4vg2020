package dev.inacap.holamundoapr4vg2020.vistas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import dev.inacap.holamundoapr4vg2020.R;
import dev.inacap.holamundoapr4vg2020.controladores.UsuariosControlador;

public class RegistrarUsuarioActivity extends AppCompatActivity {

    private EditText etNombreusuario, etPass, etPassRep;
    private Button btRegistrarme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_usuario);

        etNombreusuario = findViewById(R.id.etNombreusuario);
        etPass = findViewById(R.id.etPassword);
        etPassRep = findViewById(R.id.etPasswordRep);

        btRegistrarme = findViewById(R.id.btRegistrarme);

        btRegistrarme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombreUsuario = etNombreusuario.getText().toString();
                String password = etPass.getText().toString();
                String passRep = etPassRep.getText().toString();

                // validar que las pass coincidan
                if(!password.equals(passRep)){
                    // Mostrar mensaje de advertencia
                    etPass.setError("Estos cmpos deben coincidir");
                    etPassRep.setError("Estos cmpos deben coincidir");

                    return;
                }

                // Las pass coinciden
                UsuariosControlador capaControlador = new UsuariosControlador(getApplicationContext());
                capaControlador.crearUsuario(nombreUsuario, password);

                Toast.makeText(getApplicationContext(), "Cuenta creada", Toast.LENGTH_LONG).show();
            }
        });
    }
}
