package dev.inacap.holamundoapr4vg2020.vistas.fragmentos;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import dev.inacap.holamundoapr4vg2020.R;

public class BienvenidaFragment extends Fragment {

    private OnFragmentInteractionListener listenerInterno;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Asociar el archivo xml

        View vistaDelFragment = inflater.inflate(R.layout.fragment_bienvenida, container, false);

        // Instanciar el boton de prueba
        Button btPrueba = vistaDelFragment.findViewById(R.id.btPrueba);

        btPrueba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getContext(), "Hola desde el Fragment", Toast.LENGTH_SHORT).show();

                // Notificar a la Activity
                //listenerInterno.onFragmentInteraction("BienvenidaFragment", "BTPRUEBA_CLICK");

                // Solicitar los datos de los indicadores economicos
                // Sevidor: mindicador.cl
                // recurso: /api
                // metodo: GET

                String url = "https://www.mindicador.cl/api";

                // Crear la solicitud
                StringRequest solicitud = new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Se ejecuto la solicitud
                                Log.i("RESPUESTA_INDICADOR", response);

                                // Procesar la respuesta
                                try {
                                    JSONObject objetoRaiz = new JSONObject(response);
                                    //String version = objetoRaiz.getString("version");

                                    JSONObject dolarJSON = objetoRaiz.getJSONObject("dolar");
                                    double valorDolar = dolarJSON.getDouble("valor");

                                    Toast.makeText(getContext(), "Dolar: " + valorDolar, Toast.LENGTH_SHORT).show();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // Algo salio mal
                            }
                        }
                );

                // Enviar la solicitud
                RequestQueue listaEspera = Volley.newRequestQueue(getContext());
                listaEspera.add(solicitud);
            }
        });

        return vistaDelFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnFragmentInteractionListener){
            listenerInterno = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " debe implementar la interfaz OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listenerInterno = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String nombreFragment, String nombreEvento);
    }
}
