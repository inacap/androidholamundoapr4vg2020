package dev.inacap.holamundoapr4vg2020;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import dev.inacap.holamundoapr4vg2020.controladores.UsuariosControlador;
import dev.inacap.holamundoapr4vg2020.vistas.HomeActivity;
import dev.inacap.holamundoapr4vg2020.vistas.RegistrarUsuarioActivity;

public class MainActivity extends AppCompatActivity {

    // Declarar variables
    EditText etNombreUsuario;
    EditText etPass;
    TextView tvRecuperarPass, tvRegistrarme;
    Button btIngresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Validar que la persona tenga una sesion abierta
        SharedPreferences sesion = getSharedPreferences("sesion", Context.MODE_PRIVATE);
        boolean yaInicioSesion = sesion.getBoolean("yaInicioSesion", false);

        if(yaInicioSesion){
            // Abrir nueva Activity
            Intent intentHomeActivity = new Intent(MainActivity.this, HomeActivity.class);
            startActivity(intentHomeActivity);
            finish();
        }

        // Instanciar variables
        etNombreUsuario = findViewById(R.id.etNombreUsuario);
        etPass = findViewById(R.id.etPassword);
        tvRecuperarPass = findViewById(R.id.tvRecuperarPass);
        btIngresar = findViewById(R.id.btIngresar);

        tvRegistrarme = findViewById(R.id.tvCrearCuenta);

        btIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Interactuar


                String nombreUsuario = etNombreUsuario.getText().toString();
                String password = etPass.getText().toString();

                UsuariosControlador capaControlador = new UsuariosControlador(getApplicationContext());
                boolean inicioSesion = capaControlador.procesarLogin(nombreUsuario, password);

                if(inicioSesion){
                    // Guardar el inicio de sesion
                    SharedPreferences sesion = getSharedPreferences("sesion", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sesion.edit();

                    // Agregar la variable
                    editor.putBoolean("yaInicioSesion", true);

                    // guardar los datos
                    editor.commit();

                    // Abrir nueva Activity
                    Intent intentHomeActivity = new Intent(MainActivity.this, HomeActivity.class);
                    startActivity(intentHomeActivity);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Datos incorrectos", Toast.LENGTH_LONG).show();
                }

            }
        });

        tvRegistrarme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, RegistrarUsuarioActivity.class);
                startActivity(i);
            }
        });
    }
}
