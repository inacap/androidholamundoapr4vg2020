package dev.inacap.holamundoapr4vg2020.modelos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class UsuariosModelo {
    private BDPrincipalHelper bdHelper;

    public UsuariosModelo(Context context){
        this.bdHelper = new BDPrincipalHelper(context);
    }

    public void crearUsuario(ContentValues usuario){
        SQLiteDatabase bd = this.bdHelper.getWritableDatabase();

        bd.insert(BDPrincipalContract.BDPrincipalUsuarios.NOMBRE_TABLA, null, usuario);
    }

    public ContentValues obtenerUsuario(String nombreUsuario){
        // Definir consulta en SQL
        String consulta = String.format("SELECT * FROM %s WHERE %s = ?",
                BDPrincipalContract.BDPrincipalUsuarios.NOMBRE_TABLA,
                BDPrincipalContract.BDPrincipalUsuarios.COLUMNA_NOMBREUSUARIO);

        // Definir parametros de la consulta SQL
        String[] parametros = new String[]{
                nombreUsuario
        };

        // Abrir la BD
        SQLiteDatabase bd = this.bdHelper.getReadableDatabase();

        // Ejecutar la consulta
        Cursor cursor = bd.rawQuery(consulta, parametros);

        // Pedirle al cursor que lea la primera fila
        if(cursor.moveToFirst()){
            // Existen datos

            // Procesar los datos, organizarlos en un objeto ContentValues

            // Extraer los datos del cursor
            String nombreUsuarioCursor = cursor.getString(cursor.getColumnIndex(BDPrincipalContract.BDPrincipalUsuarios.COLUMNA_NOMBREUSUARIO));
            String passwordCursor = cursor.getString(cursor.getColumnIndex(BDPrincipalContract.BDPrincipalUsuarios.COLUMNA_PASSWORD));

            // Crear el objeto para retornarlo
            ContentValues usuario = new ContentValues();
            usuario.put(BDPrincipalContract.BDPrincipalUsuarios.COLUMNA_NOMBREUSUARIO, nombreUsuarioCursor);
            usuario.put(BDPrincipalContract.BDPrincipalUsuarios.COLUMNA_PASSWORD, passwordCursor);

            return usuario;

        }

        // No existen datos, el nombre de usuario no se encuentra en la tabla
        // el null significa que no existe el usuario
        return null;
    }
}
