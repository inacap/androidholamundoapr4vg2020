package dev.inacap.holamundoapr4vg2020.modelos;

import android.provider.BaseColumns;

public class BDPrincipalContract {
    private BDPrincipalContract(){

    }

    public static final String NOMBRE_BD = "BDPrincipalAPR4VG.db";
    public static final int VERSION_BD = 1;

    public static class BDPrincipalUsuarios implements BaseColumns{
        public static final String NOMBRE_TABLA = "usuarios";
        public static final String COLUMNA_NOMBREUSUARIO = "nombreusuario";
        public static final String COLUMNA_PASSWORD = "password";
    }
}
