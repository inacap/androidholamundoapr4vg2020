package dev.inacap.holamundoapr4vg2020.modelos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BDPrincipalHelper extends SQLiteOpenHelper {

    public static final String SQL_TABLA_USUARIOS =
            "CREATE TABLE " + BDPrincipalContract.BDPrincipalUsuarios.NOMBRE_TABLA + " (" +
                    BDPrincipalContract.BDPrincipalUsuarios._ID + " INTEGER PRIMARY KEY, " +
                    BDPrincipalContract.BDPrincipalUsuarios.COLUMNA_NOMBREUSUARIO + " TEXT, " +
                    BDPrincipalContract.BDPrincipalUsuarios.COLUMNA_PASSWORD + " TEXT)";

    public BDPrincipalHelper(Context ctx){
        // Context, Nombre de la BD, Factory, Version BD
        super(ctx, BDPrincipalContract.NOMBRE_BD, null, BDPrincipalContract.VERSION_BD);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // Crear tablas
        sqLiteDatabase.execSQL(SQL_TABLA_USUARIOS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Modificar las tablas segun la actualizacion
        //TODO: Implementar la migracion o modificacion de las tablas de la BD
    }
}
